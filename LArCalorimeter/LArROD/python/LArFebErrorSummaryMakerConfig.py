# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
from AthenaConfiguration.ComponentFactory import CompFactory
LArFebErrorSummaryMaker=CompFactory.LArFebErrorSummaryMaker
from LArBadChannelTool.LArBadChannelConfig import LArBadFebCfg
from AthenaCommon.Logging import logging


def LArFebErrorSummaryMakerCfg(configFlags):

    log = logging.getLogger('LArFebErrorSummaryMakerConfig')
    febSummaryMaker =LArFebErrorSummaryMaker()
    projectName=configFlags.Input.ProjectName

    streamName=configFlags.Input.ProcessingTags
    if len(streamName) > 0 and len(streamName[0])>4 and streamName[0].endswith("PEB"):
        log.info("StreamName %s suggests partial event building. Do not check for FEB completeness",str(streamName))
        febSummaryMaker.CheckAllFEB=False


    if projectName == "data_test":
        from datetime import date
        yearNumber=date.today().year-2000
        log.info("Found project name data_test, assume year number to be %d",yearNumber)
    else:
        yearNumber=int(projectName[4:6])

    if yearNumber > 20:
       febSummaryMaker.MaskFebScacStatus = [0x38680000,0x38720000]
       febSummaryMaker.MaskFebEvtId      = [0x38680000]
    else:
       febSummaryMaker.MaskFebScacStatus = [0x38080000]
       febSummaryMaker.MaskFebEvtId      = [0x38080000]   
    febSummaryMaker.WriteKey="StoreGateSvc+LArFebErrorSummary"
    # needed only if it is not in DB.
    #febSummaryMaker.MaskFebZeroSample = [0x39618000,0x39930000,0x3b1b0000,0x38db0000,0x38f60000,0x39ae8000,0x3bb90000]

    #from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    acc = LArBadFebCfg(configFlags)

    acc.addEventAlgo(febSummaryMaker)

    return acc


if __name__=="__main__":

    from AthenaConfiguration.AllConfigFlags import ConfigFlags
    from AthenaCommon.Logging import log
    from AthenaCommon.Constants import DEBUG
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior=1
    log.setLevel(DEBUG)

    from AthenaConfiguration.TestDefaults import defaultTestFiles
    ConfigFlags.Input.Files = defaultTestFiles.RAW
    ConfigFlags.lock()


    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from LArByteStream.LArRawDataReadingConfig import LArRawDataReadingCfg    

    acc=MainServicesCfg(ConfigFlags)
    acc.merge(LArRawDataReadingCfg(ConfigFlags))
    acc.merge(LArFebErrorSummaryMakerCfg(ConfigFlags))
    
    acc.run(3)
