/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/
#include "AlignCondAthTest.h"
#include "CSCConditionsTestAlgMT.h"
#include "MDTConditionsTestAlg.h"
#include "MDTConditionsTestAlgMT.h"
#include "MuonConditionsHistoSummary.h"
#include "MuonConditionsTestAlg.h"
#include "NswCondTestAlg.h"
#include "RPCStatusTestAlg.h"

DECLARE_COMPONENT(AlignCondAthTest)
DECLARE_COMPONENT(MuonConditionsTestAlg)
DECLARE_COMPONENT(MDTConditionsTestAlg)
DECLARE_COMPONENT(MDTConditionsTestAlgMT)
DECLARE_COMPONENT(CSCConditionsTestAlgMT)
DECLARE_COMPONENT(RPCStatusTestAlg)
DECLARE_COMPONENT(MuonConditionsHistoSummary)
DECLARE_COMPONENT(NswCondTestAlg)
